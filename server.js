const express = require("express");
const ejs = require("ejs");
const cors = require("cors");
const path = require("path");
require("./src/helpers/archivePosts");

const app = express();

app.use(cors({origin: "http://localhost:8000"}));

const boards = require("./src/routers/boards");
const archives = require("./src/routers/archives");
const PORT = process.env.PORT || 8000;

app.set("view engine", "ejs");
app.set("views", "./src/views")

app.use(express.urlencoded({ extended: true }));
app.use(express.json());

app.use("/image", express.static(path.join(`${__dirname}/public/images`)));
app.use("/css", express.static(path.join(`${__dirname}/public/css`)));

app.use("/archive", archives);
app.use("/", boards);

app.all("*", (req, res) => {
  res.status(404).render("4o4");
});

app.listen(PORT, () => {
  console.log(`Server up on port ${PORT}`);
});
