require("dotenv").config()

module.exports = {
    
    host: process.env.redis_host,
    port: process.env.redis_port

}
