const express = require("express");
const db = require("../database/database");
const queries = require("../database/queries/boards.queries");

// GET: all boards for main page
// Example: /a/; /g/; /news/
const getBoards = async (req, res) => {
  const { rows } = await db.query(queries.getBoards);
  
  await res.status(200).render("boards", { boards: rows });
};

// GET: threads in a board
const getThreads = async (req, res) => {
  const { board } = req.params;

  /*
  * Get board description
  *  We are not doing this in one query because when a board doesn't have any threads we can't get 'board_name'
  */
  const { rows: board_desc } = await db.query(queries.getBoardDesc, [board]);

  // If we don't find the board, return 404
  if (!board_desc[0]) {
    return res.status(404).render("4o4");
  }
  
  // Get threads from given board and render ejs with the data we received from the query
  const { rows: posts } = await db.query(queries.getThreads, [board]);
  await res.status(200).render("threads", { board_desc, posts, board });
};

// GET: contents of a thread
// Thread title, content, image, date of creation, replies etc.
const threadContent = async (req, res) => {
  const { board, thread_id } = req.params;

  try {

    // Find thread from its id and board and get contents of it
    const { rows: thread_content } = await db.query(queries.getThreadContent, [
      board,
      thread_id,
    ]);
    // If we can't find the thread check archives to see if its archived or not
    // If its not archived, which means it doesn't exist it will give you a sweet Four-o-Four.
    if (!thread_content[0]) {
      return res.redirect(`/archive/${board}/thread/${thread_id}`);
    }
    // Render ejs with the data we received from the query
    await res.status(200).render("post", { post: thread_content });
  } catch (err) {
      console.error(`~${err.code} ${err}`);
      res.status(404).render("4o4");
  }
};

// POST: a new thread
const createThread = async (req, res) => {
  const { board } = req.params;
  const { content, title } = req.body;
  const username = req.body.username || "Anon"; // If the user did not provide a name, set it to 'Anon'

  // If the user did not provide an image, tell them to provide a fucking image.
  if (!req.file) {
    return res.status(409).send("God damit this is an imageboard you need a fucking image.");
  }

  // Get image name
  const { filename: image } = req.file;
  
  try{
    // Make a query to database to create a new thread
    const { rows } = await db.query(queries.createThread, [
      board,
      image,
      title,
      content,
      username,
    ]);

    res.status(200).send("OK");
  } catch(err) {

    if (err.code === "22001"){ // Code 22001 means that input is longer than expected 
      return res.status(400).send("Input is longer than expected")      
    }

    console.error(`~${err.code} ${err}`)
    res.status(404).render("4o4")
  }
};

// POST: a new reply to a thread
const replyToThread = async (req, res) => {
  const { thread_id } = req.params;
  const { content } = req.body;
  const username = req.body.username || "Anon"; // If the user did not provide a name, set it to 'Anon'
  
  /*
  * If the user did not provide an image, set it to an empty string
  * If we don't do this its value will be null, and when we send image name to database it will set image name as "null" in database
  */
  const { filename: image } = req.file || "";

  if (!image && !content){
    return res.status(400).send("You can't send an empty reply.")
  }

  try {

    const { rows } = await db.query(queries.replyThread, [
      thread_id,
      username,
      content,
      image,
    ]);

    res.status(200).send("OK");
  } catch (err) {

    if (err.code === "22001"){ // Code 22001 means that input is longer than expected 
      return res.status(400).send("Input is longer than expected")      
    }

    console.error(`~${err.code} ${err}`)
    res.status(404).render("4o4")
  }
};

module.exports = {
  getBoards,
  getThreads,
  threadContent,
  createThread,
  replyToThread,
};
