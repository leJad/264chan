const express = require("express");
const db = require("../database/database");
const queries = require("../database/queries/archives.queries");

// GET: archived threads in a board
const getArchivedThreads = async (req, res) => {
  const { board } = req.params;

  /* 
  * Get board description
  * We are not doing this in one query because when a board doesn't have any threads we can't get 'board_name'
  */
  const { rows: board_desc } = await db.query(queries.getBoardDesc, [board]);

  // If we don't find the board, return 404
  if (!board_desc[0]) {
    return res.status(404).render("4o4");
  }
  
  // Get threads from given board and render ejs with the data we received from the query
  const { rows: posts } = await db.query(queries.getArchivedThreads, [board]);
  await res.status(200).render("archives/archived_threads", { board_desc, posts, board });
};

// GET: contents of an archived thread
// Thread title, content, image, date of creation, replies etc.
const getArchivedThreadContent = async (req, res) => {
  const { board, thread_id } = req.params;
  
  try {

    // Find thread from its id and board and get contents of it
    const { rows: thread_content } = await db.query(
      queries.getArchivedThreadContent,
      [board, thread_id]
    );


    // If we can't find the thread render the sweet sweet Four-o-Four page.
    if (!thread_content[0]){ 
      return res.status(404).render("4o4");
    }

    // Render ejs with the data we received from the query
    await res.status(200).render("archives/archived_post", {
      post: thread_content,
    });
  } catch (err) {
      console.error(`~ ${err}`);
      res.status(404).render("4o4");
  }
};


// GET: results of a search query
const search = async (req, res) => {
  let { title } = req.query // Get search input from query
  const { board } = req.params

  title = title.replaceAll(" ", "|") // If we don't replace blank spaces, postgres will complain

  try{

    const { rows: results } = await db.query(queries.search, [title])

    res.status(200).render("archives/search", {posts: results, board})
  } catch (err) {
    /*
    * Code 42601 means that our query (not sql query) is not right.
    * You may receive this error if your query has blank spaces
    */
    if(err.code === "42601"){
      console.error(`~${err.code} ${err}`)
      return res.status(404).send("Your input is not right.")    
    }
    
    console.error(`~${err.code} ${err}`)
    res.status(404).render("4o4")
  }

}

module.exports = {
  getArchivedThreads,
  getArchivedThreadContent,
  search,
};
