const Redis = require("ioredis");
const config = require("../configs/redis.config")

const redis = new Redis({
  port: config.port,
  host: config.host,
});

redis.on("error", (err) => {
  // If redis server is not open, throw an error
  if (err.errno === -111) {
    console.error("Couldn't connect to Redis server.");
    throw err;
  }

  throw err;
});

module.exports = redis;
