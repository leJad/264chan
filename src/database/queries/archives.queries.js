const getBoardDesc = `SELECT board_desc FROM boards WHERE board_name = $1`;

const getArchivedThreads = `
SELECT
        COUNT (reply_id) AS reply_count,
        board_desc,
        threads.image_name, threads.date, threads.content, threads.thread_id, threads.title, threads.archived
    FROM
        boards INNER JOIN threads
            ON threads.board = boards.board_name LEFT JOIN replies
            ON replies.thread = threads.thread_id
    WHERE
        board = $1
        AND threads.archived = TRUE
    GROUP BY
        board,
        board_desc, threads.image_name, threads.date, threads.content, threads.thread_id
`;

const getArchivedThreadContent = `
SELECT
        threads.image_name as op_image, threads.username as op_username, threads.date as op_date, threads.content as op_content, threads.archived,
        replies.username, replies.content, replies.date, replies.image_name
    FROM
        threads INNER JOIN boards
            ON boards.board_name = threads.board 
           	LEFT JOIN replies
			ON replies.thread = threads.thread_id
    WHERE
        board = $1
        AND thread_id = $2
        AND threads.archived = TRUE
`;

const search = `
SELECT
        threads.image_name, threads.username, threads.title, threads.content, threads.thread_id, threads.date
    FROM
        threads
    WHERE
        to_tsvector('english', threads.title) @@ to_tsquery($1)

`

module.exports = {
  getBoardDesc,
  getArchivedThreads,
  getArchivedThreadContent,
  search,
};
