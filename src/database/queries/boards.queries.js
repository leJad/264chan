const getBoards = `SELECT board_name, board_desc FROM boards`;

const getBoardDesc = `SELECT board_desc FROM boards WHERE board_name = $1`;

const getThreads = `
SELECT
        COUNT (reply_id) AS reply_count,
        threads.image_name, threads.content, threads.thread_id, threads.title, threads.archived
    FROM
        boards INNER JOIN threads
            ON threads.board = boards.board_name LEFT JOIN replies
            ON replies.thread = threads.thread_id
    WHERE
        board = $1
        AND threads.archived = FALSE
    GROUP BY
        board,
        threads.image_name, threads.content, threads.thread_id
		  `;

const getThreadContent = `
SELECT
        threads.image_name as op_image, threads.username as op_username, threads.date as op_date, threads.content as op_content, threads.archived,
        replies.username, replies.content, replies.date, replies.image_name
    FROM
        threads INNER JOIN boards
            ON boards.board_name = threads.board 
            LEFT JOIN replies
            ON replies.thread = threads.thread_id
    WHERE
        board = $1
        AND thread_id = $2
        AND threads.archived = FALSE
`;

const createThread = `
INSERT 
	INTO threads(board, image_name, title, content, username)
	VALUES($1, $2, $3, $4, $5)
`;

const replyThread = `
INSERT 
	INTO replies(thread, username, content, image_name)
	VALUES($1, $2, $3, $4)
`;

module.exports = {
  getBoards,
  getThreads,
  getBoardDesc,
  getThreadContent,
  createThread,
  replyThread,
};
