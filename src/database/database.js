const { Pool } = require("pg");
const config = require("../configs/postgres.config")

const pool = new Pool({
  user: config.user,
  host: config.host,
  database: config.database,
  password: config.password,
  port: config.port,
  max: 20,
  connectionTimeoutMillis: 5000,
  idleTimeoutMillis: 10000,
});

module.exports = pool;
