const redis = require("../database/redisClient")

const limiter = ({hitLimit, limitExpination}) => {
	
	return async (req, res, next) => {
		const ip = `${req.method}~${req.url}~${req.ip}`
		const hits = await redis.incr(ip) // Increase 'ip' on every hit
		const ttl = await redis.ttl(ip)

		if(hits === 1){
			await redis.expire(ip, limitExpination) // If user is hitting endpoint for the first time give them an expination time
		}

		if(hits > hitLimit) {
			return res.status(503).send(`Ratelimited :( Try again in ${ttl} seconds`)
		}

		next()
	}
}


module.exports = limiter