const multer = require("multer");
const path = require("path");
const fs = require("fs");

const storage = multer.diskStorage({
  destination: "./public/images/", // Upload images to public/images/ 
  filename: function (req, file, cb) {
    // If filename already exists change the og filename to something different
    fs.exists("./public/images/" + file.originalname, function (exists) {
      let uploadedFileName = file.originalname;
      if (exists) {
        uploadedFileName = Date.now() + "." + file.originalname;
      } 

      cb(null, uploadedFileName);
    });
  },
});

const upload = multer({
  storage: storage,
  limits: {fileSize: 3000000}, // Limit file size to 3MB 
  fileFilter: (req, file, cb) => {
    const validFileTypes = /jpg|jpeg|png/; // Create regex to match jp(e)g and png

    const imageSize = parseInt(req.headers['content-length']); // Get size of file

    // Do the regex match to check if file extenxion match
    const extname = validFileTypes.test(
      path.extname(file.originalname).toLowerCase()
    );


    if(imageSize > 3000000) {
      return cb("Error: Image size is too big")
    }

    if (extname === true) {
      // save file
      return cb(null, true);
    } else {
      // Return error message if file extension does not match
      return cb("Error: Images Only!");
    }

  },
}).single("image");

module.exports = {
  upload,
};
