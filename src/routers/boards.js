const controller = require("../controllers/boards.controller");
const express = require("express");
const image = require("../middlewares/imageUploader");
const ratelimiter = require("../middlewares/rateLimiter");

const router = express.Router();

router.get("/", ratelimiter({hitLimit:120, limitExpination: 30}), controller.getBoards);

router.get("/:board", ratelimiter({hitLimit:120, limitExpination: 30}), controller.getThreads);
router.post("/:board", [ image.upload, ratelimiter({hitLimit:10, limitExpination: 60}) ], controller.createThread);

router.get("/:board/thread/:thread_id", ratelimiter({hitLimit:120, limitExpination: 30}), controller.threadContent);
router.post("/:board/thread/:thread_id", [ image.upload, ratelimiter({hitLimit:15, limitExpination: 60}) ], controller.replyToThread);

module.exports = router;