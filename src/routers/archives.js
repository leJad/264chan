const controller = require("../controllers/archives.controller");
const express = require("express");
const ratelimiter = require("../middlewares/rateLimiter");

const router = express.Router();

router.get("/:board", ratelimiter({hitLimit:120, limitExpination: 30}), controller.getArchivedThreads);
router.get("/:board/thread/:thread_id", ratelimiter({hitLimit:120, limitExpination: 30}), controller.getArchivedThreadContent);
router.get("/:board/search", ratelimiter({hitLimit:120, limitExpination: 30}), controller.search);

module.exports = router;
