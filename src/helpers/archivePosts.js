const db = require("../database/database");

// Archives every unarchived post at the end of the day
const archivePosts = setInterval(async () => {
  await db.query(`
		UPDATE threads
		SET archived = TRUE
		WHERE archived = FALSE
		`);
}, 86400000); // 86400000 = 24H

module.exports = archivePosts;
