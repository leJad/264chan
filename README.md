2^64chan
============
![Banner](https://i.imgur.com/z2hWnmx.png)

Yet another 4chan clone.
---

## Built With
* [NodeJS](https://nodejs.org/en/)
* [PostgreSQL](https://www.postgresql.org/)
* [Redis](https://redis.io/)

---

## Setup
- Install Node, Postgres and Redis to your machine
- Paste queries from `src/database/model.sql` (or just use `psql -h <host> -d <table-name> -f <src/database/models.sql>`) to your DB in order to create required tables
- Clone this repo to your machine and run `npm install` to install all the dependencies
- Lastly, you might want to look into `.env` to add your database credentials.

---

## Usage
Once the dependencies are installed, you can run  `npm start` to start the server and `redis-server` to start the redis server. 

You will then be able to access it at `localhost:8000` (if you are trying it on localhost)

---

## License
>You can check out the full license [here](https://gitlab.com/leJad/264chan/-/blob/master/LICENSE)

This project is licensed under the terms of the **MIT** license.

---

## Roadmap

See [TODO](https://gitlab.com/leJad/264chan/-/blob/master/TODO)
